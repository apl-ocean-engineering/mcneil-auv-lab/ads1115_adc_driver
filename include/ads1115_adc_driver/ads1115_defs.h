/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#define ADS1115_POINTER_REGISTER 0b0
#define ADS1115_CONFIG_REGISTER 0b1

// On write, begin conversion
#define ADS1115_START_CONVERSION (0b1 << 15)  // 0 is no-op on write

// MUX configuration; bits 12-14 of config
// Report AIN0 relative to AIN1
#define ADS1115_MUX_P0_N1 (0b000 << 12)
// Report AIN{0,1,2} relative to AIN3
#define ADS1115_MUX_P0_N3 (0b001 << 12)
#define ADS1115_MUX_P1_N3 (0b010 << 12)
#define ADS1115_MUX_P2_N3 (0b011 << 12)
// Report AIN{0,1,2,3} relative to GND
#define ADS1115_MUX_P0_GND (0b100 << 12)
#define ADS1115_MUX_P1_GND (0b101 << 12)
#define ADS1115_MUX_P2_GND (0b110 << 12)
#define ADS1115_MUX_P3_GND (0b111 << 12)

// Gain Configuration (Full-Scale); bits 9-11 of config
#define ADS1115_GAIN_6V (0b000 << 9)    // Range is +/- 6.144 V
#define ADS1115_GAIN_4V (0b001 << 9)    // Range is +/- 4.096 V
#define ADS1115_GAIN_2V (0b010 << 9)    // Range is +/- 2.048 V
#define ADS1115_GAIN_1V (0b011 << 9)    // Range is +/- 1.024 V
#define ADS1115_GAIN_0p5V (0b100 << 9)  // Range is +/- 0.512 V
#define ADS1115_GAIN_0p2V (0b101 << 9)  // Range is +/- 0.256 V

// We will always use single-shot mode; bit 8 of config
#define ADS1115_MODE_SINGLE_SHOT (1 << 8)
#define ADS1115_MODE_CONTINUOUS (0 << 8)

// Available sampling rates; bits 5-7 of config
#define ADS1115_SPS_8 (0b000 << 5)
#define ADS1115_SPS_16 (0b001 << 5)
#define ADS1115_SPS_32 (0b010 << 5)
#define ADS1115_SPS_64 (0b011 << 5)
#define ADS1115_SPS_128 (0b100 << 5)
#define ADS1115_SPS_250 (0b101 << 5)
#define ADS1115_SPS_475 (0b110 << 5)
#define ADS1115_SPS_860 (0b111 << 5)

// We will always disable the comparator; bits 0-4 of config
#define ADS1115_DISABLE_COMPARATOR 0b00011
