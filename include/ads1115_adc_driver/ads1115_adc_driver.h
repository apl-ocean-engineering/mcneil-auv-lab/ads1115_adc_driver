/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <map>
#include <string>

#include "ros/ros.h"

class Ads1115AdcDriver {
 public:
  Ads1115AdcDriver();
  Ads1115AdcDriver(const Ads1115AdcDriver &) = delete;
  ~Ads1115AdcDriver() = default;

  void run();

 private:
  void setupParams();
  void setupI2C();
  void readVoltage(std::string channel_name, uint16_t channel_flag);

  // i2c device to read from. e.g. /dev/i2c-1
  std::string i2c_port_;
  uint8_t i2c_fd_;
  // Device address, as seen in `i2cdetect -y 1`
  int device_address_;
  // Requested rate; tries to sample each channel at this rate
  float requested_rate_hz_;
  // How long to sleep while waiting for ADC to take measurement
  float read_delay_sec_;
  // Configured rate for the device; slowest allowed (for best data quality)
  // that will allow the system to achieve requested rate
  uint16_t rate_flag_;
  // Configured gain for the device
  uint16_t gain_flag_;
  // Maximum absolute value voltage corresponding to configured gain.
  // Reported volts = max_voltage_ * counts / 2**15, where counts is
  // twos-complement
  float max_voltage_;
  // Voltage into ADC is scaled as part of the circuitry.
  // Instrument voltage (what we report) * scale = adc_input
  std::map<std::string, float> voltage_scales_;
  // Which channel(s) to read from. e.g. P0_N1, P2_N3, P0_GND, P3_GND
  std::map<std::string, uint16_t> channel_names_flags_;

  ros::NodeHandle nh_;
  std::map<std::string, ros::Publisher> adc_pubs_;
};
