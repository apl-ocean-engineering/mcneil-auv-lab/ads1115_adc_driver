/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ads1115_adc_driver/ads1115_adc_driver.h"

#include <fcntl.h>  // O_RDWR
#include <linux/i2c-dev.h>
#include <math.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <string>
#include <vector>

#include "ads1115_adc_driver/ads1115_defs.h"
#include "apl_msgs/Adc.h"
#include "ros/ros.h"

Ads1115AdcDriver::Ads1115AdcDriver() {
  nh_ = ros::NodeHandle("~");

  setupParams();
  for (const auto& channel_name_flag : channel_names_flags_) {
    std::string topic_name = channel_name_flag.first + "/voltage";
    ros::Publisher pub = nh_.advertise<apl_msgs::Adc>(topic_name, 10);
    adc_pubs_.insert({channel_name_flag.first, pub});
  }
  setupI2C();
}

void Ads1115AdcDriver::setupParams() {
  nh_.getParam("i2c_port", i2c_port_);
  nh_.getParam("device_address", device_address_);

  std::vector<std::string> channel_names;
  nh_.getParam("channels", channel_names);
  for (const auto& channel_name : channel_names) {
    if (channel_name == "P0_N1") {
      channel_names_flags_.insert(
          std::pair<std::string, uint16_t>(channel_name, ADS1115_MUX_P0_N1));
    } else if (channel_name == "P0_N3") {
      channel_names_flags_.insert(
          std::pair<std::string, uint16_t>(channel_name, ADS1115_MUX_P0_N3));
    } else if (channel_name == "P1_N3") {
      channel_names_flags_.insert(
          std::pair<std::string, uint16_t>(channel_name, ADS1115_MUX_P1_N3));
    } else if (channel_name == "P2_N3") {
      channel_names_flags_.insert(
          std::pair<std::string, uint16_t>(channel_name, ADS1115_MUX_P2_N3));
    } else if (channel_name == "P0_GND") {
      channel_names_flags_.insert(
          std::pair<std::string, uint16_t>(channel_name, ADS1115_MUX_P0_GND));
    } else if (channel_name == "P1_GND") {
      channel_names_flags_.insert(
          std::pair<std::string, uint16_t>(channel_name, ADS1115_MUX_P1_GND));
    } else if (channel_name == "P2_GND") {
      channel_names_flags_.insert(
          std::pair<std::string, uint16_t>(channel_name, ADS1115_MUX_P2_GND));
    } else if (channel_name == "P3_GND") {
      channel_names_flags_.insert(
          std::pair<std::string, uint16_t>(channel_name, ADS1115_MUX_P3_GND));
    } else {
      ROS_ERROR_STREAM("Unrecognized channel " << channel_name);
      ros::shutdown();
    }
  }
  std::vector<float> voltage_scales;
  nh_.getParam("voltage_scales", voltage_scales);
  if (voltage_scales.size() != channel_names.size()) {
    ROS_ERROR_STREAM("Array lengths must match. voltage_scales = "
                     << voltage_scales.size()
                     << ", channel_names = " << channel_names.size());
    ros::shutdown();
  }
  for (int ii = 0; ii < channel_names.size(); ii++) {
    voltage_scales_.insert(
        std::pair<std::string, float>({channel_names[ii], voltage_scales[ii]}));
  }

  nh_.getParam("rate_hz", requested_rate_hz_);
  // We sample each channel once per cycle, so need to account for this when
  // determining the slowest ADC rate we can use. We want the slowest because
  // (we think) it'll have the least noise.
  // We sleep a little longer than necessary -- the documentation says that the
  // rate is good to +/- 10 %, so assume it may be 10% slower than spec.
  double total_rate = requested_rate_hz_ * channel_names_flags_.size();
  if (total_rate < 8) {
    rate_flag_ = ADS1115_SPS_8;
    read_delay_sec_ = 1.0 / (0.9 * 8);
  } else if (total_rate <= 16) {
    rate_flag_ = ADS1115_SPS_16;
    read_delay_sec_ = 1.0 / (0.9 * 16);
  } else if (total_rate <= 32) {
    rate_flag_ = ADS1115_SPS_32;
    read_delay_sec_ = 1.0 / (0.9 * 32);
  } else if (total_rate <= 64) {
    rate_flag_ = ADS1115_SPS_64;
    read_delay_sec_ = 1.0 / (0.9 * 64);
  } else if (total_rate <= 128) {
    rate_flag_ = ADS1115_SPS_128;
    read_delay_sec_ = 1.0 / (0.9 * 128);
  } else if (total_rate <= 250) {
    rate_flag_ = ADS1115_SPS_250;
    read_delay_sec_ = 1.0 / (0.9 * 250);
  } else if (total_rate <= 475) {
    rate_flag_ = ADS1115_SPS_475;
    read_delay_sec_ = 1.0 / (0.9 * 475);
  } else {
    if (total_rate > 860) {
      ROS_WARN_STREAM("Requested rate of "
                      << requested_rate_hz_ << " Hz for "
                      << channel_names_flags_.size()
                      << "channels. This is above the maximum supported by the "
                         "device, so actual rate will be slower");
    }
    rate_flag_ = ADS1115_SPS_860;
    read_delay_sec_ = 1.0 / (0.9 * 860);
  }

  float max_voltage;
  nh_.getParam("max_voltage", max_voltage);
  if (max_voltage > 6.144) {
    ROS_ERROR_STREAM(
        "Requested max_voltage of "
        << max_voltage
        << " V, which is above the max supported by the ADC (6.044V)");
    ros::shutdown();
  } else if (max_voltage > 4.096) {
    gain_flag_ = ADS1115_GAIN_6V;
    max_voltage_ = 6.144;
  } else if (max_voltage > 2.048) {
    gain_flag_ = ADS1115_GAIN_4V;
    max_voltage_ = 4.096;
  } else if (max_voltage > 1.024) {
    gain_flag_ = ADS1115_GAIN_2V;
    max_voltage_ = 2.048;
  } else if (max_voltage > 0.512) {
    gain_flag_ = ADS1115_GAIN_1V;
    max_voltage_ = 1.024;
  } else if (max_voltage > 0.256) {
    gain_flag_ = ADS1115_GAIN_0p5V;
    max_voltage_ = 0.512;
  } else if (max_voltage > 0) {
    gain_flag_ = ADS1115_GAIN_0p2V;
    max_voltage_ = 0.256;
  } else {
    ROS_ERROR_STREAM("Requested negative max_voltage of "
                     << max_voltage << " V. Please specify positive value");
    ros::shutdown();
  }

  ROS_WARN_STREAM("Finished setting up params.");
  ROS_WARN_STREAM("Will be reading " << channel_names_flags_.size()
                                     << " channels.");
  ROS_WARN_STREAM("For max_voltage " << std::dec << max_voltage << " flag = 0x"
                                     << std::hex << gain_flag_);

  ROS_WARN_STREAM("For requested rate "
                  << std::dec << requested_rate_hz_ << " Hz, flag = 0x"
                  << std::hex << rate_flag_ << ", and delay = " << std::dec
                  << read_delay_sec_);
}
void Ads1115AdcDriver::setupI2C() {
  i2c_fd_ = open(i2c_port_.c_str(), O_RDWR);
  if (i2c_fd_ < 0) {
    ROS_FATAL_STREAM("Could not open i2c bus at " << i2c_port_ << ". Exiting.");
    ros::shutdown();
  } else {
    ROS_INFO_STREAM("Opened I2C port at " << i2c_port_);
  }
  if (ioctl(i2c_fd_, I2C_SLAVE, device_address_) < 0) {
    ROS_FATAL_STREAM("Failed to set " << i2c_port_ << "'s device address to "
                                      << device_address_);
    ros::shutdown();
  }
}

void Ads1115AdcDriver::readVoltage(std::string channel_name,
                                   uint16_t channel_flag) {
  // Since we are running single-shot, there are three steps to each read.
  // First, we need to tell the ADC which pair of inputs to compare
  uint16_t config = ADS1115_START_CONVERSION | channel_flag | gain_flag_ |
                    ADS1115_MODE_SINGLE_SHOT | rate_flag_ |
                    ADS1115_DISABLE_COMPARATOR;

  // Write to config register
  uint8_t dev_byte = device_address_ << 1;  // final "listen" bit is low
  uint8_t msb = config >> 8;
  uint8_t lsb = config & 0xFF;
  uint8_t buffer[3];
  // buffer[0] = dev_byte;  // I think that the address is handled at a lower
  // level.
  buffer[0] = ADS1115_CONFIG_REGISTER;
  buffer[1] = msb;
  buffer[2] = lsb;
  ROS_DEBUG_STREAM("Writing to config register 0x"
                   << std::hex << unsigned(buffer[0]) << ": 0x" << unsigned(msb)
                   << ", 0x" << unsigned(lsb));
  if (write(i2c_fd_, buffer, 3) < 3) {
    ROS_ERROR("Failed to write to config buffer.");
    return;
  }
  // Write to pointer register
  uint8_t buffer2[1];
  buffer2[0] = ADS1115_POINTER_REGISTER;
  ros::Time t0 = ros::Time::now();
  if (write(i2c_fd_, buffer2, 1) < 1) {
    ROS_ERROR("Failed to write to pointer register.");
    return;
  }
  // Our best guess for "measurement time" is halfway through the measurement
  // period.
  ros::Time tt = t0 + ros::Duration(0.5 * read_delay_sec_);
  // Wait for reading to be taken!
  ros::Duration(read_delay_sec_).sleep();
  // Read from conversion register.
  // TODO(lindzey): Consider checking whether the data is ready rather than
  // simply sleeping. (Or, sleep for the minimum time, then check after that.)
  uint8_t data[2];
  if (read(i2c_fd_, &data, 2) != 2) {
    ROS_ERROR("Failed to read from digitizer.");
    return;
  }

  // Finally, convert counts to volts!
  uint16_t counts = data[0] << 8 | data[1];
  if (counts == 0x7FFF) {
    ROS_WARN("Measured voltage at or above upper limit for digitizer.");
  } else if (counts == 0x8000) {
    ROS_WARN("Measured voltage at or below lower limit for digitizer.");
  }
  int16_t signed_counts = static_cast<int16_t>(counts);
  float volts = max_voltage_ * signed_counts / pow(2, 15);
  ROS_DEBUG_STREAM("Got data for channel " << channel_name << "! "
                                           << "Counts = " << signed_counts
                                           << " and volts = " << volts);
  apl_msgs::Adc adc_msg;
  adc_msg.header.stamp = tt;
  // This driver doesn't do any averaging.
  adc_msg.counts.push_back(signed_counts);
  adc_msg.max_counts = pow(2, 15);
  adc_msg.voltage = volts / voltage_scales_[channel_name];
  adc_pubs_[channel_name].publish(adc_msg);
}

void Ads1115AdcDriver::run() {
  // Testing my includes.
  ROS_WARN_STREAM("CONFIG REGISTER: 0x" << std::hex << ADS1115_CONFIG_REGISTER);
  ROS_WARN_STREAM("MUX P1 GND: 0x" << std::hex << ADS1115_MUX_P1_GND);
  ROS_WARN_STREAM("GAIN 6V: 0x" << std::hex << ADS1115_GAIN_6V);

  ros::Rate adc_rate = ros::Rate(requested_rate_hz_);
  while (ros::ok()) {
    for (const auto& name_flag : channel_names_flags_) {
      readVoltage(name_flag.first, name_flag.second);
    }
    adc_rate.sleep();
  }
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "ads1115_adc_driver");
  Ads1115AdcDriver adc;
  adc.run();
  return 0;
}
