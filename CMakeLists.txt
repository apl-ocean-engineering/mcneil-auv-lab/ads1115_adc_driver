cmake_minimum_required(VERSION 3.0.2)
project(ads1115_adc_driver)

add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
  apl_msgs
  roscpp
)

catkin_package(
 INCLUDE_DIRS include
 CATKIN_DEPENDS roscpp
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

add_executable(ads1115_adc_driver src/ads1115_adc_driver.cpp)

add_dependencies(ads1115_adc_driver
  ${${PROJECT_NAME}_EXPORTED_TARGETS}
  ${catkin_EXPORTED_TARGETS}
)

target_link_libraries(ads1115_adc_driver
  ${catkin_LIBRARIES}
)

install(TARGETS ads1115_adc_driver
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_ads1115_adc_driver.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
